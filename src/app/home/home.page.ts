import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  age: number;
  upper_limit: number;
  lower_limit: number;

  constructor() {
  this.age = 0;
  this.upper_limit = 0;
  this.lower_limit = 0;
  }

  calculate() {
    this.upper_limit = (220 - this.age) * 0.85;
    this.lower_limit = (220 - this.age) * 0.65;
  }
}
